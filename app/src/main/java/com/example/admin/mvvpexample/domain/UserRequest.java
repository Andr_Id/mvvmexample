package com.example.admin.mvvpexample.domain;

public class UserRequest {

    public void load(final Callback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    callback.loadedUser(mockUser());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    callback.error();
                }
            }
        }).start();
    }

    private User mockUser() {
        User user = new User();
        user.setFirstName("Andrew");
        user.setLastName("Kravet");
        return user;
    }

    public interface Callback {
        void error();

        void loadedUser(User user);
    }
}
