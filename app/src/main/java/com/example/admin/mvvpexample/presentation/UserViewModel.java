package com.example.admin.mvvpexample.presentation;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.util.Log;

import com.example.admin.mvvpexample.MainActivity;
import com.example.admin.mvvpexample.domain.User;
import com.example.admin.mvvpexample.domain.UserRequest;

public class UserViewModel {

    private UserRequest request;

    public ObservableField<String> firstName = new ObservableField<>();
    public ObservableField<String> lastName = new ObservableField<>();
    public ObservableBoolean isLoading = new ObservableBoolean();

    public UserViewModel(UserRequest request) {
        this.request = request;
    }

    public void requestUser() {
        isLoading.set(true);
        request.load(new UserRequest.Callback() {
            @Override
            public void error() {
                isLoading.set(false);
                //view.error();
            }

            @Override
            public void loadedUser(User loadedUser) {
                isLoading.set(false);
                firstName.set(loadedUser.getFirstName());
                lastName.set(loadedUser.getLastName());
            }
        });
    }
}