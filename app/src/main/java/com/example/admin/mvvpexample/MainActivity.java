package com.example.admin.mvvpexample;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.admin.mvvpexample.databinding.ActivityMainBinding;
import com.example.admin.mvvpexample.domain.UserRequest;
import com.example.admin.mvvpexample.presentation.UserViewModel;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MyLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        UserViewModel userViewModel = new UserViewModel(new UserRequest());
        binding.setUserViewModel(userViewModel);
    }
}
