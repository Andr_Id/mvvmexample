package com.example.admin.mvvpexample.presentation;

import com.example.admin.mvvpexample.domain.User;
import com.example.admin.mvvpexample.domain.UserRequest;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserViewModelTest {

    @Test
    public void sdfslkjs() {
        UserRequest request = mockSuccessRequest();
        UserViewModel viewModel = new UserViewModel(request);

        viewModel.requestUser();

        Assert.assertEquals("Andrew", viewModel.firstName.get());
        Assert.assertEquals("Kravets", viewModel.lastName.get());
    }

    private UserRequest mockSuccessRequest() {

        UserRequest request = mock(UserRequest.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                User user = mockUser();

                Object[] args = invocation.getArguments();
                ((UserRequest.Callback) args[args.length - 1]).loadedUser(user);
                return null;
            }
        }).when(request).load(any(UserRequest.Callback.class));
        return request;
    }

    private User mockUser() {
        User user = mock(User.class);
        when(user.getFirstName()).thenReturn("Andrew");
        when(user.getLastName()).thenReturn("Kravets");
        return user;
    }
}